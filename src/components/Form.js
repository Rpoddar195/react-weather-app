import React from 'react';
const Form = (props) => {
  return (
      <form onSubmit={props.loadWeather}>
        <input type="text" name="keyword" placeholder="Keyword/Location..." />
        <button>Get Weather</button>
      </form>
  )
}
export default Form;