import React from 'react';



const Weather = (props) => {
  
  return(
   // <div class="card" style="width: 18rem;" >
   // <img class="card-img-top" src="..." alt="Card image cap">
   // <div class="card-body">
   //   <h5 class="card-title">Card title</h5>
   //   <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
   //   <a href="#" class="btn btn-primary">Go somewhere</a>
   // </div>
   // </img>
   // </div>

<div class="card" style={{width: "18rem"}} >
<img class="card-img-top" src = {`${props.icon}`} alt=" "/>
<div class="card-body">
<p class="card-text">
    {props.country && <p>Location: {props.city},    {props.country}</p>}
    {props.temperature && <p>Temperature: {props.temperature}</p>}
    {props.humidity && <p>Humidity: {props.humidity}</p>}
    {props.description && <p>Conditions:  {props.description}</p>}
    {props.error && <p>{props.error}</p>}
    </p>
   </div>
  </div>
   )
}



export default Weather;
