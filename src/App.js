import React, { Component } from 'react';
import './App.css';
import Titles from './components/Titles.js';
import Form from './components/Form.js';
import Weather from './components/Weather.js';




class App extends Component {
  state = {
    temperature: undefined,
    city: undefined,
    country: undefined,
    humidity: undefined,
    description: undefined,
    icon: undefined,
    error: undefined
  }

  getWeather = async (e) => {
    e.preventDefault();
    const Api_Key = 'f6ca5b7a84089f8c0b8b5d23d811d5c2';
    const city = e.target.elements.city.value;

    const country = e.target.elements.country.value;
  
    const api_call = await fetch(`http://api.openweathermap.org/data/2.5/weather?q=${city},${country}&appid=${Api_Key}`);
    
    const response = await api_call.json();
    
    this.setState({
      temperature: response.main.temp,
      city: response.name,
      country: response.sys.country,
      humidity: response.main.humidity,
      description: response.weather[0].description,
      icon: response.weather[0].icon,
      error: ""
    })

    console.log(response);
    
    
    
  }
  render() {
   
    return (
      <div className="App">
        <Titles />
        <Form loadWeather = {this.getWeather} />
        <Weather 
          temperature={this.state.temperature}
          city={this.state.city}
          country={this.state.country}
          humidity={this.state.humidity}
          description={this.state.description}
         icon = {`http://openweathermap.org/img/w/${this.state.icon}.png`}
          error={this.state.error} />
          

      </div>
    );
  }
}

export default App;
